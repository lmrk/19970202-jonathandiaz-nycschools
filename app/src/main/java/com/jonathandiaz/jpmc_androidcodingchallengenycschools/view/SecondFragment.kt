package com.jonathandiaz.jpmc_androidcodingchallengenycschools.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.databinding.FragmentSecondBinding
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.api.ApiInterface
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.model.AverageSatScores
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.model.HighSchoolDbn
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.viewmodel.SATScoresRecyclerAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SecondFragment : Fragment() {

    companion object {
        fun newInstance() = SecondFragment()
    }

    // View Binding
    private lateinit var binding: FragmentSecondBinding

    private lateinit var satScoresRecyclerAdapter: SATScoresRecyclerAdapter
    private var schoolName: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // taking school's name
        schoolName = activity?.intent?.extras?.getString("schoolName");


        satScoresRecyclerAdapter =
            context?.let { SATScoresRecyclerAdapter(it, requireActivity()) }!!
        binding.recyclerview.layoutManager = LinearLayoutManager(context)
        binding.recyclerview.adapter = satScoresRecyclerAdapter

        satScores()
    }

    private fun satScores() {
        val apiInterface = ApiInterface.create().getSAT()

        apiInterface.enqueue(object : Callback<List<AverageSatScores>> {
            override fun onResponse(
                call: Call<List<AverageSatScores>>,
                response: Response<List<AverageSatScores>>
            ) {
                for (i in 0 until response.body()?.size!!) {
                    val getId = HighSchoolDbn(response.body()!![i].id)
                    if (response.body() != null && getId.id == schoolName) {
                        satScoresRecyclerAdapter.setSATScoreListItems(response.body()!![i])
                        break
                    } else {

                    }
                }
            }

            override fun onFailure(call: Call<List<AverageSatScores>>, t: Throwable) {

            }
        })
    }
}