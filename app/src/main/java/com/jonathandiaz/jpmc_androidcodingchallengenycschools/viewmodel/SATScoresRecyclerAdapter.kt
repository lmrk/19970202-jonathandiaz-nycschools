package com.jonathandiaz.jpmc_androidcodingchallengenycschools.viewmodel

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.R
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.databinding.SatScoresRecyclerviewAdapterBinding
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.model.AverageSatScores
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.view.MainFragment

class SATScoresRecyclerAdapter(val context: Context, var fragmentActivity: FragmentActivity) :
    RecyclerView.Adapter<SATScoresRecyclerAdapter.MyViewHolder>() {

    var satScoreList: List<AverageSatScores> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            SatScoresRecyclerviewAdapterBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return satScoreList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.binding.dbn.text = "Dbn: \n" + satScoreList[position].id
        holder.binding.math.text = "Math Score: \n" + satScoreList[position].math
        holder.binding.reading.text = "Reading Score: \n" + satScoreList[position].reading
        holder.binding.writing.text = "Writing Score: \n" + satScoreList[position].writing

    }

    fun setSATScoreListItems(satScoreList: AverageSatScores) {
        this.satScoreList = listOf(satScoreList)
        notifyDataSetChanged()
    }

    inner class MyViewHolder(val binding: SatScoresRecyclerviewAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.backBtn.setOnClickListener {
                fragmentActivity.supportFragmentManager.popBackStack(
                    null,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE
                )

                fragmentActivity
                    .supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container_view, MainFragment.newInstance())
                    .addToBackStack(null)
                    .commit()
            }
        }
    }


}