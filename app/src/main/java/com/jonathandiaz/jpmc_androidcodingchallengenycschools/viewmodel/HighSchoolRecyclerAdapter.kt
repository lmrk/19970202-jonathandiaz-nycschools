package com.jonathandiaz.jpmc_androidcodingchallengenycschools.viewmodel

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.R
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.databinding.RecyclerviewAdapterBinding
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.model.Highschool
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.view.SecondFragment

class HighSchoolRecyclerAdapter(val context: Context, var fragmentActivity: FragmentActivity) : RecyclerView.Adapter<HighSchoolRecyclerAdapter.MyViewHolder>() {


    var highSchoolNamesList: List<Highschool> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            RecyclerviewAdapterBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return highSchoolNamesList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.binding.dbn.text = highSchoolNamesList[position].id
        holder.binding.schoolName.text = highSchoolNamesList[position].name

    }

    fun setHighSchoolNamesListItems(highSchoolNamesList: List<Highschool>) {
        this.highSchoolNamesList = highSchoolNamesList
        notifyDataSetChanged()
    }

    inner class MyViewHolder(val binding: RecyclerviewAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.imageButtonPlayBtn.setOnClickListener {
                Log.d("Clicked", "it was clicked")

                fragmentActivity.intent.putExtra("schoolName", binding.dbn.text)

                fragmentActivity.supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

                fragmentActivity
                    .supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container_view, SecondFragment.newInstance())
                    .addToBackStack(null)
                    .commit()
            }
        }

    }
}